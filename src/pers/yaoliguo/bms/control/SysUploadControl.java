package pers.yaoliguo.bms.control;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping(value="/UploadControl")
public class SysUploadControl {

	protected Logger logger = Logger.getLogger(SysUploadControl.class);
	
	@RequestMapping(value="/skipFileUpload")
	public String skipFileUpload(){
		
		return "redirect:/views/uploadFlie/upload.html";
	}
	
	
	@RequestMapping(value="/uploadFile")
	@ResponseBody
	public Object uploadImg(MultipartFile file,HttpServletRequest request, HttpServletResponse response){
		
		JSONObject obj = new JSONObject();
		//获取前台传来的目录
		String folder = request.getParameter("folder");
		if(folder == null || "".equals(folder)){
			folder = "default";
		}
		// 支持的图片格式
		String[] fileExts = { "jpg", "png", "gif","jpeg","bmp","zip"};
		//获取upload文件夹绝对路径
		String path = request.getSession().getServletContext().getRealPath("/upload");
		//获取文件名
		String fileName = file.getOriginalFilename();
		//获取文件格式
		String format = fileName.substring(fileName.lastIndexOf(".")+1);
		Boolean permit = false;
		//判断上传的文件格式是否是允许上传的文件
		for(String fileFormat : fileExts){
			if(fileFormat.equalsIgnoreCase(format))
			{
				permit = true;
			}
		}
		
		if(!permit){
			obj.put("result", "error");
			obj.put("name", "");
			obj.put("info", "不支持上传"+format+"格式的文件");
			return obj;
		}
		//生成报存的文件名
		String saveFileName = UUID.randomUUID().toString().trim().replaceAll("-", "")+"."+format;
		File fileFolder = new File(path+File.separator+folder);
		File saveFile = new File(fileFolder+File.separator+saveFileName);
		
		if(!fileFolder.exists()){
			fileFolder.mkdirs();
		}
		
		InputStream inputStream = null;
		OutputStream outputStream = null;
		//读写缓存
		byte []buf = new byte[1024];
		int lenth = 0;
		try {
			inputStream = file.getInputStream();
			outputStream = new FileOutputStream(saveFile);
			//循环读写
			while ((lenth = inputStream.read(buf)) != -1) {
				outputStream.write(buf, 0, lenth);
			}
			outputStream.flush();
			obj.put("result", "success");
			obj.put("name", saveFileName);
		} catch (IOException e) {
			e.printStackTrace();
			obj.put("result", "error");
			obj.put("name", "");
			obj.put("info", "服务器发生异常了！");
		}finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(outputStream != null){
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return obj;
		
	}
	
}
