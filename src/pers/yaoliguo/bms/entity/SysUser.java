package pers.yaoliguo.bms.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import pers.yaoliguo.bms.uitl.DateUtil;
import pers.yaoliguo.bms.uitl.DateYMDHMSJsonDeserializer;
import pers.yaoliguo.bms.uitl.DateYMDHMSJsonSerializer;

public class SysUser implements Serializable{
    private String id;

    private String userName;

    private String userNameEn;

    private String account;

    private String password;

    private String sex;

    private String email;

    private String tel;

    private String roleId;

    private String organizId;
    
    @DateTimeFormat(pattern=DateUtil.DATE_FORMAT_TIME_T)  
    private Date createTime;

    private String createUesrId;

    @DateTimeFormat(pattern=DateUtil.DATE_FORMAT_TIME_T)
    private Date updateTime;

    private String updateUesrId;

    private Integer seq;

    private Boolean del;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserNameEn() {
        return userNameEn;
    }

    public void setUserNameEn(String userNameEn) {
        this.userNameEn = userNameEn == null ? null : userNameEn.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public String getOrganizId() {
        return organizId;
    }

    public void setOrganizId(String organizId) {
        this.organizId = organizId == null ? null : organizId.trim();
    }
    @JsonSerialize(using = DateYMDHMSJsonSerializer.class)   
    public Date getCreateTime() {
        return createTime;
    }
   
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUesrId() {
        return createUesrId;
    }
    
    public void setCreateUesrId(String createUesrId) {
        this.createUesrId = createUesrId == null ? null : createUesrId.trim();
    }
    @JsonSerialize(using = DateYMDHMSJsonSerializer.class)   
    public Date getUpdateTime() {
        return updateTime;
    }
    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUesrId() {
        return updateUesrId;
    }

    public void setUpdateUesrId(String updateUesrId) {
        this.updateUesrId = updateUesrId == null ? null : updateUesrId.trim();
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }


    public Boolean getDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }
}