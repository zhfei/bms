package pers.yaoliguo.bms.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;
@Repository("SysRoleMenuDao")
public interface SysRoleMenuDao {
    int deleteByPrimaryKey(SysRoleMenuKey key);

    int insert(SysRoleMenuKey record);

    int insertSelective(SysRoleMenuKey record);
    
    List<SysRoleMenuKey> getrolemenus(SysRoleMenuKey record);
    
    int deleteByRoleId(SysRoleMenuKey key);
}