package pers.yaoliguo.bms.uitl;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import pers.yaoliguo.bms.aspect.DynamicDataSourceHolder;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年7月31日 下午4:48:13 
* 类说明 
*/
public class DynamicSourceMnager extends AbstractRoutingDataSource{

	@Override
	protected Object determineCurrentLookupKey() {
		// TODO Auto-generated method stub
		// 从自定义的位置获取数据源标识
        return DynamicDataSourceHolder.getDataSource();
	}
	
	
}
