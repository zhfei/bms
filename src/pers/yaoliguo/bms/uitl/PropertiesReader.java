package pers.yaoliguo.bms.uitl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @ClassName:       PropertiesReader
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年8月20日        下午10:26:05
 */
public class PropertiesReader {
	/**
	 * 读取默认config.properties配置项
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public synchronized static String getConfigValue(String key)throws IOException {  
        Properties props = new Properties();  
        InputStream in = null;  
        try {  
            in = PropertiesReader.class.getResourceAsStream("/process.properties"); 
            props.load(in);  
            String value = props.getProperty(key);  
            return value;  
  
        } catch (IOException e) {  
            e.printStackTrace();  
            return null;  
  
        } finally {  
            if (null != in)  
                in.close();  
        }  
    } 
	
	/**
	 * 读取*.properties配置项
	 * @param fileName
	 * @param key
	 * @return
	 * @throws IOException
	 */
    public synchronized static String getValue(String fileName, String key)throws IOException {  
        Properties props = new Properties();  
        InputStream in = null;  
        try {  
            in = PropertiesReader.class.getResourceAsStream(File.separator+fileName); 
            props.load(in);  
            String value = props.getProperty(key);  
            return value;  
  
        } catch (IOException e) {  
            e.printStackTrace();  
            return null;  
  
        } finally {  
            if (null != in)  
                in.close();  
        }  
    }
    
    public static void main(String[] a) throws IOException{
    	String val = PropertiesReader.getConfigValue("LeavaBill");
    	System.out.println(val);
    }
}
