var login1 = new Vue({
	el:"#login",
	data:{
		userView:{
			account:"root",
			password:"root",
			code:""
		},
		info:"",
		postURL:baseUtil.WebRootUrl+"/LoginControl/login/auth",
		codeUrl:"../CodeControl/code?rand="+Math.random()
	},
	methods:{
		submit:function(){
			vm = this;
			vm.$http.post(this.postURL,this.userView,{emulateJSON:true})
			.then(function(response){
				vm.info = response.data.info;
				vm.$message({
			          showClose: true,
			          message: vm.info
			        });
				
				if(response.data.result == "200"){
					window.location.href = "index.html";
				}
				vm.changeCode();
			})
			.catch(function(response){
				vm.info = "有异常了！";
				vm.$message({
			          showClose: true,
			          message: vm.info
			        });
				vm.changeCode();
			})
			
		},
		changeCode:function(){
			this.codeUrl = "../CodeControl/code?rand="+Math.random();
		}
	}
});
if(window.top != window.self){
	top.location.href = location.href;  
}